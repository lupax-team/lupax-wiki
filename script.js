// Build "Contents" Box (use h2 tags in content)
function buildContentsBox(){
    const contents_box = document.getElementById('contents');
    const contents_box_links = contents_box.querySelector('.links');
    const headline_elements = document.querySelectorAll('#content h1, #content h2:not(:empty)');

    if(!Array.from(headline_elements).filter(h => h.tagName == 'H2').length)
        return;

    headline_elements.forEach((h2, key) => {
        let id = "h2-headline-" + key;
        if(h2.hasAttribute('id') && h2.getAttribute('id'))
            id = h2.getAttribute('id');
        else
            h2.setAttribute('id', id);

        let h2_link = document.createElement('a');
        h2_link.innerText = h2.innerText;
        h2_link.setAttribute('href', '#' + id);
        h2_link.dataset.id = id;
        contents_box_links.appendChild(h2_link);
    });

    document.body.classList.add('contents-visible');
    console.log(document.body.classList);
    contents_box.classList.add('show');
    document.addEventListener('scroll', handleActiveLinkInContentsBox);
    document.addEventListener('resize', saveHeadlinePositionsToContentsBox);

    saveHeadlinePositionsToContentsBox();
    handleActiveLinkInContentsBox();
}

function saveHeadlinePositionsToContentsBox(){
    const h2_link_elements = document.querySelectorAll('#contents a');
    h2_link_elements.forEach(link => {
        const h2_element = document.getElementById(link.dataset.id);
        const h2_top = h2_element.getBoundingClientRect().top;
        const body_top = document.body.getBoundingClientRect().top;
        link.dataset.top = h2_top - body_top;
    })
}


function handleActiveLinkInContentsBox(){
    let scroll_top = document.documentElement.scrollTop;
    let new_active_link;

    const h2_link_elements = Array.from(document.querySelectorAll('#contents a'));

    // scolled to top -> set active to first element
    if(scroll_top < 100){
        new_active_link = h2_link_elements[0];
    // scrolled to bottom -> set active to last element
    } else if(Math.abs(document.documentElement.scrollHeight - document.documentElement.scrollTop - document.documentElement.clientHeight) < 1){
        new_active_link = h2_link_elements[h2_link_elements.length - 1];
    } else {
        let new_active_link_index = h2_link_elements.findIndex(headline => headline.dataset.top > scroll_top + 200);
        if(new_active_link_index > 0) new_active_link_index -= 1;
        else if(new_active_link_index < 0) new_active_link_index = h2_link_elements.length - 1;

        new_active_link = h2_link_elements[new_active_link_index];
    }

    if(new_active_link){
        let current_active_link = document.querySelector('#contents a.active');
        if(current_active_link) current_active_link.classList.remove('active');

        new_active_link.classList.add('active');

    }
}

buildContentsBox();
