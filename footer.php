			</section>
            <aside id="contents">
                <h3>Inhalt</h3>
                <div class="links"></div>
            </aside>
		</main>
        <footer id="footer">
            <div class="main-wrapper">
                © Copyright <?= date('Y') ?> - Lupax Software
				<?php wp_nav_menu(array(
					'theme_location' => 'footer'
				)); ?>
            </div>
        </footer>
	<?php wp_footer(); ?>
    </body>
</html>
