<!DOCTYPE html>
<html <?php language_attributes(); ?> class="<?= is_admin_bar_showing() ? 'admin-bar' : '' ?>">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

		<title><?php the_title(); ?></title>

		<?php /* FONTS */ ?>
		<!--<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Roboto+Slab&display=swap" rel="stylesheet">-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
		<header id="header">
            <div class="main-wrapper">
    			<img src="https://wiki.lupax.org/wp-content/uploads/2019/06/Lupax-Logo-white_5000-300x133.png" alt="Logo"/>
    			<div class="title">Wiki</div>
            </div>
		</header>
		<main id="main" class="main-wrapper">
            <aside id="sidebar">
    			<nav id="nav">
    				<?php wp_nav_menu(array(
						'theme_location' => 'sidebar'
					)); ?>
    			</nav>
    		</aside>
			<section id="content">
