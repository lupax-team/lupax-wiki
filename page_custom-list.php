<?php /* Template Name: List */
get_header();


while(have_posts()) : the_post(); ?>
	<h1 id="title"><?php the_title(); ?></h1>
	<?php the_content(); ?>

	<div class="page-list">
		<?php
		// $all_pages = get_pages(array('post_type' => 'page'));
		// $children = get_page_children(get_the_ID(), $all_pages);
		$children = get_pages( array(
		    'parent'      => is_front_page() ? 0 : get_the_ID(),
		    'sort_column' => 'menu_order',
		) );

		foreach($children as $child){
			if($child->ID == get_the_ID()) continue;
			
			$icon = get_post_meta($child->ID, 'jf_icon', true); ?>
			<a class="page-item card" href="<?= get_the_permalink($child) ?>">
				<header>
					<i class="fa fa-<?= $icon ?: 'circle-info' ?>"></i>
					<?= get_the_title($child) ?>
				</header>
				<section class="excerpt">
					<?= get_the_excerpt($child) ?>
				</section>
			</a>
		<?php } ?>
	</div>
<?php endwhile;
get_footer();
