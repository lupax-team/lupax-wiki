<?php
// -------------------------------------------------------------------------------- //
// --------------------------------- MENU ----------------------------------------- //
// -------------------------------------------------------------------------------- //
register_nav_menus(array(
	'sidebar' => 'Menu in Sidebar'
));
register_nav_menus(array(
	'footer' => 'Menu in Footer'
));

// Show Icon of page in nav menu
function jfShowIconInMenuForPages( $menu_objects, $args ) {

    // print_r($menu_objects);
    foreach ( $menu_objects as $key => &$menu_object ) {
        $icon = get_post_meta($menu_object->object_id, 'jf_icon', true);
        if($icon) $menu_object->title = "<i class='fa fa-$icon'></i>$menu_object->title";
    }

    return $menu_objects;
}
add_filter( 'wp_nav_menu_objects', 'jfShowIconInMenuForPages', 10, 2 );


// -------------------------------------------------------------------------------- //
// -------------------------------- STYLES ---------------------------------------- //
// -------------------------------------------------------------------------------- //
function registerStyles(){
	// General CSS
	wp_enqueue_style('style', get_stylesheet_uri(), array(), '1.0.4');
}
add_action('wp_enqueue_scripts', 'registerStyles');

// -------------------------------------------------------------------------------- //
// ------------------------------- SCRIPTS ---------------------------------------- //
// -------------------------------------------------------------------------------- //
function registerScripts(){
    // General JS
    wp_enqueue_script('script', get_stylesheet_directory_uri() . '/script.js', array(), '1.0.4', array('in_footer' => true));
}
add_action('wp_enqueue_scripts', 'registerScripts');

// -------------------------------------------------------------------------------- //
// ----------------------------- META BOXES --------------------------------------- //
// -------------------------------------------------------------------------------- //
add_post_type_support( 'page', 'excerpt' );
// Icon for Pages
function jfAddIconMetaBoxForPages() {
    add_meta_box('jf-icon', 'Icon', 'jfIconMetaBoxForPagesHtml', 'page', 'side', 'high');
}
function jfIconMetaBoxForPagesHtml( $post ) {
    // $values = get_post_custom( $post->ID );
    // $icon = isset( $values['jf_icon'] ) ? esc_attr( $values['jf_icon'][0] ) : '';
    $icon = get_post_meta($post->ID, 'jf_icon', true);
    wp_nonce_field('jf_icon_save', 'jf_icon_nonce');
    ?>
    <div style="display: flex; column-gap: 10px; align-items: center;">
        <label for="jf_icon">Icon</label>
        <input type="text" name="jf_icon" id="jf_icon" value="<?= $icon ?>" onchange="console.log(this, this.nextSibling); this.nextSibling.className = 'fa fa-' + this.value;"/><i class="fa fa-<?= $icon ?: 'question' ?>"></i>
    </div>

    <?php
}
add_action('add_meta_boxes', 'jfAddIconMetaBoxForPages');
function jfIconMetaBoxSave( $post_id ) {
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if( !isset( $_POST['jf_icon_nonce'] ) || !wp_verify_nonce( $_POST['jf_icon_nonce'], 'jf_icon_save' ) ) return;
    if( !current_user_can( 'edit_post', $post_id ) ) return;
    if( isset( $_POST['jf_icon'] ) )
        update_post_meta( $post_id, 'jf_icon', $_POST['jf_icon'] );
}
add_action( 'save_post', 'jfIconMetaBoxSave' );
